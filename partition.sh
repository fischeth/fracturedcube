#!/usr/bin/bash

number_of_partitions=$1

BIN_PATH=$HOME/w/o/cr/bin

if [ ! -d ${number_of_partitions} ];
then
    mkdir ${number_of_partitions}
fi

if [ ! -e Ra_795_fault_with_ic_on_fault.mesh ];
then
    ${BIN_PATH}/partmesh --ogs2metis -i Ra_795_fault_with_ic_on_fault.vtu
fi

if [ ! -e ${number_of_partitions}/Ra_795_fault_with_ic_on_fault.mesh ];
then
    cd ${number_of_partitions}
    ln -s ../Ra_795_fault_with_ic_on_fault.mesh Ra_795_fault_with_ic_on_fault.mesh
    cd ..
fi

${BIN_PATH}/partmesh -m -n ${number_of_partitions} -i Ra_795_fault_with_ic_on_fault -o ${number_of_partitions} -- Ra_795_fault_Bottom.vtu Ra_795_fault_POINT0.vtu Ra_795_fault_POINT1.vtu Ra_795_fault_POINT2.vtu Ra_795_fault_POINT3.vtu Ra_795_fault_Top.vtu

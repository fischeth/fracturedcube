#!/usr/bin/env bash

# Create folder for the sources
BASE_DIR=$1
OGS_DIR=${BASE_DIR}/w/o

mkdir -p ${OGS_DIR}
cd ${OGS_DIR}
if [[ ! -d s ]];
then
    git clone --single-branch --branch SCADS https://gitlab.opengeosys.org/TobiasMeisel/ogs.git/ s
fi

# Build the ogs binary

## Preliminary: Build PETSc package on TAURUS
### Load modules necessary for PETSc / OGS builds

module load CMake/3.18.4-GCCcore-10.2.0
module load git
module load GCC/9.3 Eigen/3.3.7-GCCcore-9.3.0 Boost/1.72.0-gompi-2020a
module load ScaLAPACK/2.1.0-gompi-2020a
module load OpenMPI/4.0.3-GCC-9.3.0
module load HDF5/1.10.6-gompi-2020a

### Clone PETSc / build PETSc / install PETSc locally

echo "#########################################################"
echo "### Clone PETSc / build PETSc / install PETSc locally ###"
echo "#########################################################"

cd ${BASE_DIR}/w
if [[ ! -d petsc ]];
then
    git clone -b release https://gitlab.com/petsc/petsc.git petsc
fi
cd petsc
./configure --with-cc=gcc --with-cxx=g++ --with-fc=0 --download-make --prefix=${BASE_DIR} --with-debugging=0
make
make PETSC_DIR=${BASE_DIR}/w/petsc PETSC_ARCH=arch-linux-c-opt install

## Build OGS
### Load modules necessary to build OGS

module load VTK/8.2.0-foss-2020a-Python-3.8.2
module load Ninja

### Configure OGS / build OGS

echo "#############################"
echo "### Configure / build OGS ###"
echo "#############################"

if [[ -d ${OGS_DIR}/br_petsc ]];
then
    rm -f ${OGS_DIR}/br_petsc
fi

mkdir ${OGS_DIR}/br_petsc
cd ${OGS_DIR}/br_petsc
CC=`which mpicc` CXX=`which mpic++` cmake ../s/ -G Ninja -DCMAKE_BUILD_TYPE=Release -DOGS_EIGEN_DYNAMIC_SHAPE_MATRICES=Off -DOGS_USE_PCH=Off -DOGS_USE_PETSC=On -DPETSC_DIR=${BASE_DIR}/w/petsc -DPETSC_ARCH=arch-linux-c-opt -DOGS_USE_MPI=On -DOGS_USE_CONAN=OFF -DCPM_SOURCE_CACHE=$HOME/.cache/CPM -DOGS_BUILD_PROCESSES=HT

ninja ogs


# Clone and Build OpenGeoSys

Usually, I create a folder 'w' for the source code I'm working on.
In the following description this is used sometimes.

```bash
~ $ mkdir -p w/o
```

## Checkout the soucres

Clone the sources within the 'w/o' folder into the sub-folder 's':
```bash
~/w/o $ git clone https://gitlab.opengeosys.org/ogs/ogs.git s
```

## Optionally modify sources

For HPC there might be a problem that the chunks written are too small.
Chunk_space in HdfData can (currently only hardcoded) be increased. 
Example: 
from
>     chunk_space =
>         (size_tuple > 1)
>             ? std::vector<Hdf5DimType>{partition_info.longest_local_length,
>                                        size_tuple}
>             : std::vector<Hdf5DimType>{partition_info.longest_local_length};

to 

>     chunk_space =
>         (size_tuple > 1)
>             ? std::vector<Hdf5DimType>{partition_info.longest_local_length*16,
>                                        size_tuple}
>             : std::vector<Hdf5DimType>{partition_info.longest_local_length*16};

## Build the binary

### Preliminary: Build PETSc package on TAURUS

Clone and build PETSc into the 'w' folder:
```bash
~/w $ git clone -b release https://gitlab.com/petsc/petsc.git petsc
cd petsc
```
ToDo Tom: Configure and compile as batch job on the target architecture
```
./configure --with-cc=gcc --with-cxx=g++ --with-fc=0 --download-make --prefix=${HOME} --with-debugging=0
make
make PETSC_DIR=$HOME/w/petsc PETSC_ARCH=arch-linux-c-debug install
```

### Build OpenGeoSys on TAURUS

The following modules are needed:
```
module load GCC/9.3 Eigen/3.3.7-GCCcore-9.3.0 Boost/1.72.0-gompi-2020a
module load VTK/8.2.0-foss-2020a-Python-3.8.2
module load OpenMPI/4.0.3-GCC-9.3.0
module load ScaLAPACK/2.1.0-gompi-2020a
module load HDF5/1.10.6-gompi-2020a
```

In the folder 'o' I create a sub-folder 'br_petsc' (build
release using PETSc).

More information how to build OpenGeoSys can be found here:
https://www.opengeosys.org/docs/devguide/getting-started/introduction/

```bash
CC=`which mpicc` CXX=`which mpic++` cmake ../s/ -G Ninja -DCMAKE_BUILD_TYPE=Release -DOGS_EIGEN_DYNAMIC_SHAPE_MATRICES=Off -DOGS_USE_PCH=Off -DOGS_USE_PETSC=On -DPETSC_DIR=${BASE_DIR}/w/petsc -DPETSC_ARCH=arch-linux-c-opt -DOGS_USE_MPI=On -DOGS_USE_CONAN=OFF -DCPM_SOURCE_CACHE=$HOME/.cache/CPM -DOGS_BUILD_PROCESSES=HT
```

# Checkout the Example Simulation Model

```bash
~ $ mkdir data
cd data
```

```bash
~/data $ git clone https://git.ufz.de/fischeth/fracturedcube.git
```
or
```bash
~/data $ git clone git@git.ufz.de:fischeth/fracturedcube.git
```

# Run the Example Simulation Model

Two submit scripts are provided: [ogs_cube_ht_benchmark_rev0](rev0/24/ogs_cube_ht_benchmark_rev0.sh) and [ogs_cube_ht_benchmark_rev0_xdmf](rev0/24/ogs_cube_ht_benchmark_rev0_xdmf.sh).

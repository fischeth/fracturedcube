#!/bin/bash
#SBATCH -J Benchmark
#SBATCH -p haswell128
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=24
#SBATCH --cpus-per-task=1
#SBATCH --exclusive    # ensure that nobody spoils my measurement on 2 x 24 x 1 cores
#SBATCH --mail-user=thfi710b@tu-dresden.de
#SBATCH --output /scratch/ws/0/thfi710b-ogs_cube_ht_benchmark_rev0/%j-out.txt
#SBATCH --error /scratch/ws/0/thfi710b-ogs_cube_ht_benchmark_rev0/%j-err.txt
#SBATCH --time=00:15:00

module load GCC/9.3 Eigen/3.3.7-GCCcore-9.3.0 Boost/1.72.0-gompi-2020a
module load VTK/8.2.0-foss-2020a-Python-3.8.2
module load OpenMPI/4.0.3-GCC-9.3.0
module load ScaLAPACK/2.1.0-gompi-2020a
module load HDF5/1.10.6-gompi-2020a

# add for PETSc
LD_LIBRARY_PATH=$LD_LIBRARY_PATH:/home/thfi710b/lib

mkdir -p /scratch/ws/0/thfi710b-ogs_cube_ht_benchmark_rev0/${SLURM_JOB_NAME}/${SLURM_JOB_ID}

srun /home/thfi710b/w/o/br_petsc/bin/ogs /home/thfi710b/data/cube_ht_benchmark/rev0/48/Ra_795_fault_bcgs_jacobi_xdmf.prj -o /scratch/ws/0/thfi710b-ogs_cube_ht_benchmark_rev0/${SLURM_JOB_NAME}/${SLURM_JOB_ID}/

mv /scratch/ws/0/thfi710b-ogs_cube_ht_benchmark_rev0/${SLURM_JOB_ID}-out.txt /scratch/ws/0/thfi710b-ogs_cube_ht_benchmark_rev0/${SLURM_JOB_NAME}/${SLURM_JOB_ID}/out.txt
mv /scratch/ws/0/thfi710b-ogs_cube_ht_benchmark_rev0/${SLURM_JOB_ID}-err.txt /scratch/ws/0/thfi710b-ogs_cube_ht_benchmark_rev0/${SLURM_JOB_NAME}/${SLURM_JOB_ID}/err.txt

